var vrednost = ''

$(document).ready(function() {
    $('.hamburger').click(function() {
        $(this).toggleClass('open');
        $('.nav').toggleClass('opened');
        if ($('.nav').hasClass('opened')) {
            // dodat overflowhidden kako se strana ne bi skrolovala dok je otvoren meni
            $('body').addClass('overflowhidden')
        } else {
            $('body').removeClass('overflowhidden')
        }
    });
    // Apendovanje dva diva u footeru
    if ($(window).width() < 992) {
        $('.footercontenttwo').append($('.footercontentfour'))
        $('.footercontenttwo').append($('.footercontentfive'))
    }
    $('#loaderwrap').delay(1000).fadeOut()
    $('.headinginputcontent').delay(1600).fadeIn()
    $('.chooseanimate').delay(1600).addClass('slideup')
});

$(window).scroll(function() {
    if ($(window).scrollTop() > '10') {
        $('#indexpage #header').addClass('darkblue')
    } else {
        $('#indexpage #header').removeClass('darkblue')
    }
})
// Inicijalizacija slajdera
$('.sliderholder').slick({
    dots: true,
    arrows: true,
    infinite: true,
    autoplay: false,
    speed: 3000,
    appendArrows: $('.headinginputcontent'),
    appendDots: $('.inputsholder'),
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [{
        breakpoint: 768,
        settings: {
            arrows: false,
            appendDots: $('.inputsholder'),
        }
    }]
})

// F-ja koja otvara lazni select na naslovnoj strani
function showmetypes() {
    $('.divtypes ul').toggleClass('hidden')
}
// f-ja za jezicki dropdown
$('.lingua').click(function(e) {
    e.stopPropagation();
    $('.lingua ul').slideToggle(250);
});
$('body').click(function() {
    $('.lingua ul').slideUp(250);
});

// f-ja koja menja boldovanu cenu
function billingtime(elem) {
    vrednost = ''
    vrednost = $(elem).prop('checked')
    if ($(window).width() < 992) {
        if (vrednost == true) {
            $('#mobile-pricing .whiteprop .monthbill').removeClass('bolded')
            $('#mobile-pricing .whiteprop .yearbill').addClass('bolded')
        } else {
            $('#mobile-pricing .whiteprop .yearbill').removeClass('bolded')
            $('#mobile-pricing .whiteprop .monthbill').addClass('bolded')
        }
    } else {
        if (vrednost == true) {
            $('#desktop-pricing .whiteprop .monthbill').removeClass('bolded')
            $('#desktop-pricing .whiteprop .yearbill').addClass('bolded')
        } else {
            $('#desktop-pricing .whiteprop .yearbill').removeClass('bolded')
            $('#desktop-pricing .whiteprop .monthbill').addClass('bolded')
        }
    }
}
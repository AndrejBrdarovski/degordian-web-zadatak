var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat-css');
var imagemin = require('gulp-imagemin');
var merge = require('merge-stream');
var cssMin = require('gulp-css')
var minify = require('gulp-minify');

gulp.task('compressjs', function() {
    return gulp.src(['./js/main.js'])
        .pipe(minify())
        .pipe(gulp.dest('dist'))
});

gulp.task('sass', function() {
    return gulp.src('scss/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('css'))
})
gulp.task('sass-responsive', function() {
    return gulp.src('scss/responsive.scss')
        .pipe(sass())
        .pipe(gulp.dest('css'))
})
gulp.task('main-css', function() {
    return gulp.src([
            './scss/style.scss',
            './scss/responsive.scss'
        ])
        .pipe(concat('main.css'))
        .pipe(gulp.dest('css'))
})
gulp.task('opt-imgs', function() {
    return gulp.src('img/*')
        .pipe(imagemin({
            interlaced: true,
            progressive: true,
            optimizationLevel: 5,
        }))
        .pipe(gulp.dest('dist/img'))
})
gulp.task('concatizujbezmin', function() {
    return gulp.src([
            './css/style.css',
            './css/responsive.css',
            './css/slick.css',
            './css/slick-theme.css'
        ])
        .pipe(concat("style.css"))
        .pipe(gulp.dest('./css'));
});
gulp.task('concatizuj', function() {
    return gulp.src([
            './css/style.css',
            './css/responsive.css'
        ])
        .pipe(concat("style.min.css"))
        .pipe(cssMin())
        .pipe(gulp.dest('./dist'));
});
// gulp.task('smanjicss', function() {
//     return gulp.src([
//             './css/style.css',
//         ])
//         .pipe(cssMin("proba.min.css") {
//             keepSpecialComments: 0;
//         })
//         .pipe(gulp.dest('./dist'));
// });



//pregledaj kasnije
// gulp.task('spajanje', function() {
//     var sassStream,
//         cssStream;
//
//     sassStream = gulp.src('./scss/style.scss')
//         .pipe(sass({
//             errLogToConsole: true
//         }));
//
//     cssStream = gulp.src('./scss/responsive.scss');
//
//     return merge(sassStream, cssStream)
//         .pipe(concat('main.css'))
//         .pipe(gulp.dest('css'));
// });